$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "subscribem/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "subscribem"
  s.version     = Subscribem::VERSION
  s.authors     = ["mazharoddin"]
  s.email       = ["mazharoddin@gmail.com"]
  s.homepage    = "http://mazharuddin.com"
  s.summary     = "Summary of Subscribem."
  s.description = "Description of Subscribem."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency "thor"
  s.add_dependency "bcrypt"
  s.add_dependency "warden"
  s.add_dependency "dynamic_form"
  s.add_dependency "sqlite3"
  s.add_dependency "houser"
  s.add_dependency "braintree"
  s.add_dependency "pry"

  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
  s.add_development_dependency "guard"
  s.add_development_dependency "guard-livereload"
  s.add_development_dependency "guard-rspec"
  s.add_development_dependency "database_cleaner"
  s.add_development_dependency "factory_girl"
end
